# Ejercicio Postulación Global Logic

_Se presenta una solución, desarrollada en springboot, consiste en un api rest para creación de usuarios y su CRUD._

## Tecnologias. 🚀
    ```
    - Springboot.
    - Gradle 3.x
    - JWT
    - Java 1.8
    - JUnit
    - Mockito
    - JPA
    - Base de datos en memoria (H2)
    ```

## IDE Utilizado. ☕
    ```
    - Eclipse.
    ```

### Pre-requisitos 📋
    ```
    - Java 8
    - Postman
    ```
### Instalación 🔧

_Para ejecutar este proyecto debes seguir las siguientes instrucciones:_

    ```
    - Abrir una consola de comandos
    - Ubicarse dentro de la carpeta del proyecto ahí econtraras la carpeta de Documentacion, en donde se encontrara el jar del proyecto
    - Escribir la siguiente linea de comandos (windows) 'java -jar globalbci-0.0.1-SNAPSHOT', para asi levantar el proyecto.
    - Abrir postman
    - importar la coleccion que se encuentra dentro de la carpeta documentación en donde encontraras las funcionalidades de la api(GET, PUT, DELETE, POST)
    - el proyecto se despliega en el puerto 8081
    ```


### Ejecutando las pruebas en Postman⚙️

_solo debes usar la api rest que se encuentra dentro de la colección que ya importaste. solo enviar las peticiones,_ 

o cambiar la informacion del JSON para probar las validaciones.

### Diagramas 📖

_Dentro de la carpeta documentación estan los diagramas solicitados: Diagrama de componentes y de secuencias._

### Autor ✒️

_Cristian Donoso Cubillos.-_
