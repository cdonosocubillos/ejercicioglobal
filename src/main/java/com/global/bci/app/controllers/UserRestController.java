package com.global.bci.app.controllers;

import java.sql.Date;
import java.time.Instant;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.global.bci.app.entity.User;
import com.global.bci.app.service.IUserService;
import com.global.bci.app.service.impl.UserServiceImpl;
import com.global.bci.app.utils.ValidatorsUtil;

import net.bytebuddy.implementation.bytecode.Throw;


@RestController
@RequestMapping(value = {"/api/users"})
public class UserRestController {

	private Logger logger = LoggerFactory.getLogger(UserRestController.class);
	
	@Autowired
	private IUserService userService;

	@PostMapping()
	public ResponseEntity<?> create(@Valid @RequestBody User user, BindingResult result){
		
		User newUser = null;
		Map<String, Object> response = new HashMap<>();
		
		if(result.hasErrors()) {

			List<String> errors = result.getFieldErrors()
					.stream()
					.map(err -> "El campo '" + err.getField() +"' "+ err.getDefaultMessage())
					.collect(Collectors.toList());
			
			response.put("errors", errors);
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.BAD_REQUEST);
		}
		
		try {
			if (ValidatorsUtil.validateMail(user.getEmail()) && ValidatorsUtil.validatePass(user.getPassword())) {
				user.setCreateAt(Date.from(Instant.now()));
				user.setLastLoginAt(Date.from(Instant.now()));
				user.setIsActive(true);
				newUser = userService.save(user);
			}else {
				logger.error("Formato de contrasenia incorrecto");
				response.put("Error", "Formato de contrasenia incorrecto");
				return new ResponseEntity<Map<String, Object>>(response, HttpStatus.BAD_REQUEST);
			}
		} catch(DataAccessException e) {
			logger.error("Ups!. Ocurrio un error al insertar en la base de datos.");
			response.put("mensaje", "Ups!. Ocurrio un error al insertar en la base de datos.");
			response.put("error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		logger.info("Usuario creado correctamente");
		response.put("mensaje", "El usuario ha sido creado con exito!");
		response.put("User", newUser);
		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.CREATED);
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<?> findById(@PathVariable Long id) {
		User user = null;
		Map<String, Object> response = new HashMap<>();
		try {
			user = userService.findById(id);
		} catch(DataAccessException e) {
			logger.error("Ups!. Ocurrio un error al realizar la consulta en la base de datos!: " + e.getMessage());
			response.put("mensaje", "Ups!. Ocurrio un error al realizar la consulta en la base de datos!");
			response.put("error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		if(user==null) {
			logger.error("El usuario no se encuentra en la base de datos.");
			response.put("mensaje", "El usuario ID: ".concat(id.toString().concat(" no existe en la base de datos!")));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<User>(user, HttpStatus.OK);
	}
	
	@GetMapping()
	public List<User> findAll() {
		return userService.findAll();
	}
	
	@PutMapping("/{id}")
	public ResponseEntity<?> update(@Valid @RequestBody User user, BindingResult result, @PathVariable Long id) {

		User currentUser = userService.findById(id);
		User userUpdated = null;
		Map<String, Object> response = new HashMap<>();

		if(result.hasErrors()) {

			List<String> errors = result.getFieldErrors()
					.stream()
					.map(err -> "El campo '" + err.getField() +"' "+ err.getDefaultMessage())
					.collect(Collectors.toList());
			
			response.put("errors", errors);
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.BAD_REQUEST);
		}
		
		if (currentUser == null) {
			response.put("mensaje", "Ups! Ocurrio un error: no se pudo editar al usuario ID: "
					.concat(id.toString().concat(" no existe en la base de datos!")));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.NOT_FOUND);
		}

		try {

			if (ValidatorsUtil.validateMail(user.getEmail()) && ValidatorsUtil.validatePass(user.getPassword())) {
				currentUser.setName(user.getName());
				currentUser.setEmail(user.getEmail());
				currentUser.setPassword(user.getPassword());
				currentUser.setIsActive(user.getIsActive());
				currentUser.setUpdateAt(Date.from(Instant.now()));
				userUpdated = userService.save(currentUser);
			}else {
				logger.error("Formato de contrasenia incorrecto");
				response.put("Error", "Formato de contrasenia incorrecto");
				return new ResponseEntity<Map<String, Object>>(response, HttpStatus.BAD_REQUEST);
			}
		} catch (DataAccessException e) {
			logger.error("Ups!. Ocurrio un error al actualizar en la base de datos.");
			response.put("mensaje", "Error al actualizar al usuario en la base de datos");
			response.put("error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		logger.info("Usuario actualizado correctamente");
		response.put("mensaje", "El Usuario ha sido actualizado con éxito!");
		response.put("usuario", userUpdated);

		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.CREATED);
	}
	
	@DeleteMapping("/{id}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public ResponseEntity<?> delete(@PathVariable(name = "id") Long id) {
		Map<String, Object> response = new HashMap<>();
		
		try {
			
			User user = userService.findById(id);
			if (user != null) {
				userService.delete(id);
			}
			
		} catch (DataAccessException e) {
			logger.error("Ups!. Ocurrio un error al eliminar al usuario de la base de datos: " + e.getMessage());
			response.put("mensaje", "Ups!. Ocurrio un error al eliminar al usuario de la base de datos");
			response.put("error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		logger.info("Usuario eliminado con exito");
		response.put("mensaje", "El usuario ha sido eliminado con exito!");
		
		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.OK);
	}
	
}
